package com.basicoop.cookies

class CookieScriptMain {

    static main(args) {
        Map cookieJar = [cookies:[]]
        
        cookieJar.cookies << [cookieType: 'chocolat']
        cookieJar.cookies << [cookieType: 'chocolat']
        cookieJar.cookies << [cookieType: 'chocolat']
        cookieJar.cookies << [cookieType: 'apple cookie']
        cookieJar.cookies << [cookieType: 'bisquit']
        cookieJar.cookies << [cookieType: 'pretzel']
        
        println "jar: ${cookieJar}"
        
        int chocolatCookieCount = cookieJar.cookies.findAll {it.cookieType == 'chocolat'}.size()
        
        println "${chocolatCookieCount} chocolat cookies"
        
        Map cookie = cookieJar.cookies.find { it.cookieType == 'pretzel'}
        
        if(cookie) {
            cookieJar.cookies.remove(cookie)
            println "eating ${cookie.cookieType}"
        }
        
        println "jar: ${cookieJar}"
    }

}
