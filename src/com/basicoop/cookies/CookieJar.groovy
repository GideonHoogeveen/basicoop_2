package com.basicoop.cookies

import groovy.transform.ToString

@ToString(includePackage=false)
class CookieJar {
    
    List<Cookie> cookies
    
    public CookieJar() {
        cookies = []
    }
    
    public void addCookie(Cookie cookie) {
        cookies << cookie
    }
    
    public boolean hasCookie() {
        return !cookies.isEmpty()
    }
    
    public boolean hasCookie(CookieType cookieType) {
        return cookies.find {it.cookieType == cookieType} ? true : false
    }
    
    public Cookie takeCookie() {
        if(!cookies) {
            throw new JarIsEmptyException('You tried to take a cookie from the jar, but there were no cookies left!')
        }
        
        return cookies.remove(0)
    }
    
    public Cookie takeCookie(CookieType cookieType) {
        if(!cookies) {
            throw new JarIsEmptyException('You tried to take a cookie from the jar, but there were no cookies left!')
        }
        
        Cookie cookie = cookies.find {it.cookieType == cookieType}
        
        if(!cookie) {
            throw new CookieNotFoundException("No cookies with cookie type ${cookieType} have been found in this jar.")
        }
        
        cookies.remove(cookie)
        return cookie
    }
    
    public int countCookies() {
        return cookies.size()
    }
    
    public int countCookies(CookieType cookieType) {
        return cookies.findAll {it.cookieType == cookieType}.size()
    }
    
    
}
