package com.basicoop.cookies

class JarIsEmptyException extends RuntimeException {
    
    public JarIsEmptyException(String message) {
        super(message)
    }
    
}
