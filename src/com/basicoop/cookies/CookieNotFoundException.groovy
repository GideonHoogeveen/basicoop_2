package com.basicoop.cookies

class CookieNotFoundException extends RuntimeException {
    
    public CookieNotFoundException(String message) {
        super(message)
    }
    
}
