package com.basicoop.cookies

class CookieJarMain {
    public static void main(String[] args) {
        CookieJar cookieJar = new CookieJar()
        cookieJar.addCookie(new Cookie(CookieType.APPLE_COOKIE))
        cookieJar.addCookie(new Cookie(CookieType.APPLE_COOKIE))
        cookieJar.addCookie(new Cookie(CookieType.APPLE_COOKIE))
        cookieJar.addCookie(new Cookie(CookieType.BISQUIT))
        cookieJar.addCookie(new Cookie(CookieType.CHOCOLAT_CHIP))
        cookieJar.addCookie(new Cookie(CookieType.PRETZEL))
        
        println cookieJar
        
        while(cookieJar.hasCookie(CookieType.APPLE_COOKIE)) {
            cookieJar.takeCookie(CookieType.APPLE_COOKIE)
        }
        
        println cookieJar
        
    }
}
