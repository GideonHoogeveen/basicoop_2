package com.basicoop.cookies

enum CookieType {
    CHOCOLAT_CHIP, APPLE_COOKIE, BISQUIT, PRETZEL
}
