package com.basicoop.cookies

import groovy.transform.ToString

@ToString(includePackage=false)
class Cookie {
    
    CookieType cookieType
    
    public Cookie(CookieType cookieType) {
        this.cookieType = cookieType
    }
    
}
