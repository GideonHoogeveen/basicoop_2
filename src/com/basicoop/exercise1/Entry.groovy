package com.basicoop.exercise1

import groovy.transform.ToString

@ToString(includeNames=true, includePackage = false)
class Entry {
    
    String text
    String author
    
    public Entry(String text, String author) {
        this.text = text
        this.author = author
    }
    
    public String prettyOutput() {
        "${text} \n    ${author}"
    }

}
