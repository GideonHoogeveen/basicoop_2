package com.basicoop.exercise1

import com.sun.xml.internal.ws.client.sei.ResponseBuilder.StringBuilder;

import groovy.transform.ToString

@ToString(includeNames=true,includePackage = false)
class Logbook {
    
    String name
    List<Entry> entries = []
    
    public Logbook(String name) {
        this.name = name
    }
    
    public void addEntry(Entry entry) {
        entries.add(entry)
    }
    
    public String prettyOutput() {
        String prettyOutput = "<<< ${name} >>>\n\n"
        
        entries.each { Entry entry ->
            prettyOutput += entry.prettyOutput() + '\n\n'
        }
        
        return prettyOutput
        
    }
}
