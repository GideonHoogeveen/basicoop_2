package com.basicoop.exercise1

class LogbookMain {

    static main(args) {
        
        Writer karel = new Writer('Karel')
        Writer daphne = new Writer('Daphne')
        Writer anonymous = new Writer(' . . . ')
        
        Logbook logbook = new Logbook('CRV logboek')
        karel.writeInLogbook(logbook, "Ik ben de eerste die in het logboek schrijft")
        karel.writeInLogbook(logbook, "Maar ik schrijf lekker ook het tweede stuk")
        daphne.writeInLogbook(logbook, "Zo makkelijk kom je daar niet mee weg! Ik schrijf lekker ook!")
        anonymous.writeInLogbook(logbook, "Ik heb net een dropje gejat. Gelukkig kan niemand traceren wie ik ben!")
        
        println logbook.prettyOutput()
    }

}
