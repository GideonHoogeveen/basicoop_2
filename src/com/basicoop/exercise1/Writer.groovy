package com.basicoop.exercise1

class Writer {
    
    String name
    
    public Writer(String name) {
        this.name = name
    }
    
    public void writeInLogbook(Logbook logBook, String log) {
        logBook.addEntry(new Entry(log, name))
    }
        
}
