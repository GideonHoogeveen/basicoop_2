//Filename: Apple.groovy

//package directory.structure
package com.basicoop.whatswhat

//class ClassName
class Apple {
    
    //PropertyType propertyName
    AppleColor appleColor
    boolean hasStem
    
    //Constructor
    //acces_modifier ConstructorName(parameterType parameter, ParameterType parameter = defaultValue) {
    public Apple(AppleColor appleColor, boolean hasStem = true) {
        //appleColor of this class becomes the same as the parameter appleColor
        this.appleColor = appleColor
        this.hasStem = hasStem
    }
    
    //Method
    //acces_modifier return_type methodName(ParameterType parameter)
    public void setAppleColor(AppleColor appleColor) {
        this.appleColor = appleColor
    }
    
    //Method
    //acces_modifier return_type methodName() {
    public boolean hasStem() {
        
        //return returnValue
        return hasStem //return value should be the same type as return_type
    }
    
    /*
     * name of class, name of file and Constructor should all be the same. Should start with a Capital character.
     * methods should always start with a lowercase character
     * 
     * acces_modifier = public, private, protected etc.
     * return type = boolean, AppleColor, void etc.
     */
}
