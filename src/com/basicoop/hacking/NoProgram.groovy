package com.basicoop.hacking

class NoProgram implements Program {

    @Override
    public void execute() {
        println 'no program installed'
    }

}
