package com.basicoop.hacking

class Virus implements Program {

    @Override
    public void execute() {
        println 'Your computer belongs to me now!'
    }

}
