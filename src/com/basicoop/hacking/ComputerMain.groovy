package com.basicoop.hacking

class ComputerMain {

    static main(args) {
        
        Computer computer = new Computer()
        
        computer.executeProgram()
        
        computer.installProgram(new HelloWorldProgram())

        computer.executeProgram()
        
    }

}
