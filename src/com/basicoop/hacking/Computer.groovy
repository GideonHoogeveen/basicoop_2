package com.basicoop.hacking

class Computer {
    
    Program program = new NoProgram()
    
    public void installProgram(Program program) {
        this.program = program
    }
    
    public void executeProgram() {
        program.execute()
    }
    
}
