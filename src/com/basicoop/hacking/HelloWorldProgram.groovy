package com.basicoop.hacking

class HelloWorldProgram implements Program {

    @Override
    public void execute() {
        println 'Hello World'        
    }

}
