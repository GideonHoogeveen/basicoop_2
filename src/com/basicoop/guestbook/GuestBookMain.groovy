package com.basicoop.guestbook

class GuestBookMain {

    public static void main(args) {
        GuestBook guestBook = new GuestBook('My big fat Gideon wedding!')
        Guest gideon = new Guest('Gideon')
        Guest stefan = new Guest('Stefan')
        Guest anonymous = new Guest(' . . . ')
        
        gideon.writeInGuestBook(guestBook, "Dit is een hele leuke bruiloft!")
        stefan.writeInGuestBook(guestBook, "Zuipen!!!!")
        anonymous.writeInGuestBook(guestBook, "Ik heb snoep gestolen. hihihi")
        println guestBook.prettyOutput()
        
        new Guest('')
        
    }

}
