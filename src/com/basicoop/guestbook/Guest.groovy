package com.basicoop.guestbook

import groovy.transform.ToString

@ToString(includeNames=true, includePackage=false)
class Guest {
    
    String name
    
    public Guest(String name) {
        assert name
        this.name = name
    }
    
    public void writeInGuestBook(GuestBook guestBook, String message) {
        guestBook.addGuestBookItem(new GuestBookItem(name, message))
    }
    
}
