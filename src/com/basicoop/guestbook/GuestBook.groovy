package com.basicoop.guestbook

import groovy.transform.ToString

@ToString(includeNames=true, includePackage=false)
class GuestBook {
    
    String name
    List<GuestBookItem> guestBookItems    
    
    public GuestBook(String name) {
        this.name = name
        this.guestBookItems = []
    }
    
    public int size() {
        return guestBookItems.size()
    }
    
    public void addGuestBookItem(GuestBookItem guestBookItem) {
        guestBookItems.add(guestBookItem)
    }
    
    public String prettyOutput() {
        String prettyOutput = "<<< ${name} >>>\n\n"
        
        guestBookItems.each { GuestBookItem guestBookItem ->
            prettyOutput += guestBookItem.prettyOutput() + '\n\n'
        }
        
        return prettyOutput
    }
}
