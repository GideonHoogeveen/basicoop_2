package com.basicoop.guestbook

import groovy.transform.ToString

@ToString(includeNames=true, includePackage=false)
class GuestBookItem {

    String name
    String message
    
    public GuestBookItem(String name, String message) {
        this.name = name
        this.message = message
    }
    
    public String prettyOutput() {
        "${message} \n    ${name}"
    }
    
}
