package com.basicoop.penguins

import groovy.transform.ToString

@ToString(includeNames=true, includePackage = false)
class Egg {
    
    Penguin penguin
    
    public Egg(Penguin penguin){
        this.penguin = penguin
    }
    
    public Penguin birthPenguin() {
        return penguin
    }
    
}
