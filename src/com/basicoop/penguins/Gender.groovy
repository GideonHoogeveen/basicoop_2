package com.basicoop.penguins

enum Gender {
    FEMALE, MALE
    
    public static Gender randomGender() {
        Random random = new Random()
        int index = random.nextInt(Gender.values().size())
        return Gender.values()[index]
    }
}
