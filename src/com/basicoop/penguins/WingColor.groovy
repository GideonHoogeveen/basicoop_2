package com.basicoop.penguins

enum WingColor {
    BLACK, BLUE, YELLOW, RED
    
    public static WingColor randomWingColor() {
        Random random = new Random()
        int index = random.nextInt(WingColor.values().size())
        return WingColor.values()[index]
    }
}
