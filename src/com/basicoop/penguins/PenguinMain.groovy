package com.basicoop.penguins

class PenguinMain {

    static main(args) { 
        
        List<Penguin> penguins = []
        
        for(int i = 0; i < 10; i++) {
            penguins << PenguinFactory.createRandomPenguin()
        }
        
        for(int i = 0; i < penguins.size(); i++) {
            for(int j = 0; j < penguins.size(); j++) {
                println penguins[i].reproduce(penguins[j])
            }
        }
        
    }

}
