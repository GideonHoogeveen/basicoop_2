package com.basicoop.penguins

class PenguinFactory {
    
    public static Penguin createRandomPenguin() {
        return new Penguin(Gender.randomGender(), WingColor.randomWingColor())
    }
    
}
