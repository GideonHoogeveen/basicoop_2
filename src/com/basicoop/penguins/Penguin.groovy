package com.basicoop.penguins

import groovy.transform.ToString

@ToString(includeNames=true, includePackage = false)
class Penguin {
    
    Gender gender
    WingColor wingColor
    
    public Penguin(Gender gender, WingColor wingColor) {
        this.gender = gender
        this.wingColor = wingColor
    }
    
    public Egg reproduce(Penguin penguin) {
        if(this.gender == penguin.gender) {
            return null
        }
        
        int index = new Random().nextInt(2)
        return new Egg(new Penguin(Gender.randomGender(), [wingColor, penguin.wingColor][index]))
        
    }
    
}
