package com.basicoop.cake

class CakeBox {
    List<CakePiece> cakePieces = []
    
    public CakeBox() {
        cakePieces = []
    }
    
    public void addCakePieces(List cakePieces) {
        this.cakePieces.addAll(cakePieces)
    }
    
    public boolean isEmpty() {
        return cakePieces.isEmpty()
    }
    
    public CakePiece takePiece() {
        assert cakePieces
        return cakePieces.remove(0)
    }
    
}
