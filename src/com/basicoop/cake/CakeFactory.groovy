package com.basicoop.cake

class CakeFactory {
    
    public static createRandomCake() {
        Random random = new Random()
        CakeSize cakeSize = CakeSize.values()[random.nextInt(CakeSize.values().size())]
        CakeType cakeType = CakeType.values()[random.nextInt(CakeType.values().size())]
        return new Cake(cakeType, cakeSize)
    }
    
}
