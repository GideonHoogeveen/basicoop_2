package com.basicoop.cake

class Customer {
    
    public void eatAllCake(CakeBox cakeBox) {
        
        while(!cakeBox.isEmpty()) {
            CakePiece cakePiece = cakeBox.takePiece()
            println "I ate a piece of ${cakePiece.cakeType} cake. I am a very happy customer!"
        }
        
        println "There is no cake left. Now I'm a very sad customer.."
    }
}
