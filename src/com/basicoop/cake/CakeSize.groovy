package com.basicoop.cake

enum CakeSize {
    SMALL(1), NORMAL(2), HUGE(4)
    
    int pieces
    
    public CakeSize(int pieces) {
        this.pieces = pieces
    }
}
