package com.basicoop.cake

class CakeOOMain {

    static main(args) {
        Cake cake = CakeFactory.createRandomCake()
        List<CakePiece> pieces = cake.cutCake()
        
        CakeBox cakeBox = new CakeBox()
        cakeBox.addCakePieces(pieces)
        
        Customer customer = new Customer()
        customer.eatAllCake(cakeBox)
    }

}
