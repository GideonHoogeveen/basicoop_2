package com.basicoop.cake

class CakeMain {

    static main(args) {
        
        /* TODO: OOP2 Exercise 1.1 Cake Q1
         * Wat doet deze code?
         */
        
        /* TODO: OOP2 Exercise 1.1 Cake Q2
         * Van Taart tot Klant.
         * Welke vier gebeurtenissen zijn belangrijk in deze code?
         */
        
        /* TODO: OOP2 Exercise 1.1 Cake Q3
         * Bedenk voor elk aangegeven punt wat de aannames zijn en wat daarbij wat er mis kan gaan.
         */
        
        /* TODO: OOP2 Exercise 1.1 Cake Q4
         * Welke 'dingen uit de werkelijkheid' (entities) vind je terug in deze code?
         * Bij een winkel zou je bijvoorbeeld kunnen denken aan: Klant, Verkoper, Product, Geld, Bon
         */
        
        /* TODO: OOP2 Exercise 1.1 Cake Q5
         * Op welke eigenschappen vari�ren deze entities?
         */
        
        
        //#1
        List cakeTypes = ['Chocolat', 'cream', 'apple']
        List cakeSizes = ['small', 'normal', 'huge']
        
        Random random = new Random()
        
        //#2
        String cakeType = cakeTypes[random.nextInt(3)]
        String cakeSize = cakeSizes[random.nextInt(3)]
        
        //#3
        Map cake = [cakeType: cakeType, cakeSize: cakeSize]
        
        List pieces = []
        
        int amount = 0
        
        //#4
        switch(cake.cakeSize) {
            //#5
            case 'small':
                amount = 2
                break
            case 'huge':
                amount = 8
                break
            case 'normal':
            //#6
            default:
                amount = 4
                break                
        }
        
        //#7
       amount.times { pieces << [pieceType: cake.cakeType] }
       
       Map cakeBox = [pieces: pieces]
       
       //#8
       while(cakeBox.pieces.size() > 0) {
           //#9
           Map piece = cakeBox.pieces.remove(0)
           println "I ate a piece of ${piece.pieceType} cake. I am a very happy customer!"
       }
       
       println "There is no cake left. Now I'm a very sad customer.."
       
    }

}
