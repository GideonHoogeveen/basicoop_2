package com.basicoop.cake

class Cake {
    CakeType cakeType
    CakeSize cakeSize
    
    public Cake(CakeType cakeType, CakeSize cakeSize) {
        this.cakeType = cakeType
        this.cakeSize = cakeSize
    }
    
    public List<CakePiece> cutCake() {
        return (0..<cakeSize.pieces).collect {new CakePiece(cakeType)}
    }
    
}
